import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError } from 'rxjs';
import { Carte } from '../shared/carte.model';

@Injectable({
  providedIn: 'root',
})
export class CarteService {
  private backendUrl = 'http://localhost:8080/api/carte';

  constructor(private http: HttpClient) {}

  getMainNonTriee(): Observable<Carte[]> {
    return this.http
      .get<Carte[]>(this.backendUrl)
      .pipe(catchError(this.handleError));
  }

  getMainTriee(cartes: Carte[]): Observable<Carte[]> {
    return this.http
      .post<Carte[]>(this.backendUrl, cartes)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: any): Observable<any> {
    console.error("Une erreur s'est produite :", error);
    throw error;
  }
}
