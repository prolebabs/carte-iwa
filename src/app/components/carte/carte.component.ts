import { Component, Input, OnInit } from '@angular/core';
import { Carte } from '../../shared/carte.model';

@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.css'],
})
export class CarteComponent implements OnInit {
  @Input() carte: Carte = new Carte();

  ngOnInit(): void {}

  getLetter(value: number): string {
    if (value === 14) {
      return 'A';
    } else if (value === 13) {
      return 'K';
    } else if (value === 12) {
      return 'Q';
    } else if (value === 11) {
      return 'J';
    } else {
      return value.toString();
    }
  }
}
