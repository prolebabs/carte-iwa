import { Component, OnInit } from '@angular/core';
import { CarteService } from 'src/app/services/carte.service';
import { Carte } from 'src/app/shared/carte.model';

@Component({
  selector: 'app-list-carte',
  templateUrl: './list-carte.component.html',
  styleUrls: ['./list-carte.component.css'],
})
export class ListCarteComponent implements OnInit {
  listeCartes: Carte[] = [];
  listeCartesTriees: Carte[] = [];

  constructor(private carteService: CarteService) {}

  ngOnInit(): void {
    this.loadData();
  }

  private loadData() {
    this.carteService.getMainNonTriee().subscribe((cartes) => {
      this.listeCartes = cartes;
      this.trierMain(this.listeCartes);
    });
  }

  trierMain(cartes: Carte[]) {
    this.carteService
      .getMainTriee(cartes)
      .subscribe((data) => (this.listeCartesTriees = data));
  }

  lancerMain() {
    this.loadData();
  }
}
